#include<bits/stdc++.h>
#include<ext/pb_ds/assoc_container.hpp>
using namespace std;
using namespace __gnu_pbds;
 
typedef tree<int,null_type,less_equal<int>,rb_tree_tag,tree_order_statistics_node_update> order_set;
typedef long long  ll ;
 
 
 
#define     rep(i, begin, end)          for (__typeof(end) i = (begin) - ((begin) > (end)); i != (end) - ((begin) > (end)); i += 1 - 2 * ((begin) > (end)))
#define     what_is(x)                  cout << #x << " is " << x << endl;
// #define     endl                        '\n'
#define     fi                          first
#define     sec                         second
#define     pb                          push_back
#define     vi                          vector<int>
#define     vii                         vector<pair<int,int>>     
#define 	fio 						ios_base::sync_with_stdio(false);cin.tie(NULL);cout.tie(NULL);
#define 	sz(x) 						(int)x.size()
#define 	all(x) 						begin(x), end(x)
void solve(){
    int n;
    cin>>n;
    int g=0;
    for(int i=0;i<n;i++){
        int num;
        cin>>num;
        g=__gcd(g,num);
    }
    cout<<g<<endl;
    return;
}
int main(){
    fio;   
    int test;
    // cin>>test;
    test=1;
    while(test--){
        solve();
    }
    return 0;
}
 
 