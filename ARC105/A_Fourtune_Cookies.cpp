#include<bits/stdc++.h>
#include<ext/pb_ds/assoc_container.hpp>
using namespace std;
using namespace __gnu_pbds;
 
typedef tree<int,null_type,less_equal<int>,rb_tree_tag,tree_order_statistics_node_update> order_set;
typedef long long  ll ;
 
 
 
#define     rep(i, begin, end)          for (__typeof(end) i = (begin) - ((begin) > (end)); i != (end) - ((begin) > (end)); i += 1 - 2 * ((begin) > (end)))
#define     what_is(x)                  cout << #x << " is " << x << endl;
// #define     endl                        '\n'
#define     fi                          first
#define     sec                         second
#define     pb                          push_back
#define     vi                          vector<int>
#define     vii                         vector<pair<int,int>>     
#define 	fio 						ios_base::sync_with_stdio(false);cin.tie(NULL);cout.tie(NULL);
#define 	sz(x) 						(int)x.size()
#define 	all(x) 						begin(x), end(x)
void solve(){
    int a,b,c,d;
    cin>>a>>b>>c>>d;
    int s=a+b+c+d;
    for(int i=0;i<2;i++){
        for(int j=0;j<2;j++){
            for(int k=0;k<2;k++){
                for(int l=0;l<2;l++){
                    int val=i*a+j*b+k*c+l*d;
                    // cout<<val<<endl;
                    if(2*val==s){cout<<"Yes"<<endl;return;}
                }
            }
        }
    }
    cout<<"No"<<endl;return;

}
int main(){
    fio;   
    int test;
    // cin>>test;
    test=1;
    while(test--){
        solve();
    }
    return 0;
}
 
 